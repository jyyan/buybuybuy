<?php

$mypay_store_uid = '';
$app_aes_key = '';
$mypay_api_url = '';

function mypay_encrypt($decrypted_data, $app_aes_key)
{
  // Set cipher method
  $cipherMethod = 'AES-256-CBC';
  // The key length must be 256 bits long
  $app_aes_key= (empty($app_aes_key)) ? openssl_random_pseudo_bytes(32) : $app_aes_key;
  // Set initialization vector
  $app_aes_iv = (empty($app_aes_iv)) ? openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipherMethod)) : $app_aes_iv;
  // Encryption
  // openssl_encrypt uses PKCS#7 padding by default if the options parameter was set to zero
  $encrypted_data = openssl_encrypt($decrypted_data, $cipherMethod, $app_aes_key, OPENSSL_RAW_DATA, $app_aes_iv);

  $res = array(
    'app_aec_key' => $app_aes_key,
    'app_aec_iv' => $app_aes_iv,
    'decrypted_data' => $decrypted_data,
    'encrypted_data' => $encrypted_data,
    'encrypted_data_base64_iv' => base64_encode( $app_aes_iv . $encrypted_data),
  );
  return $res;
}

function mypay_decrypt($encrypted_data_base64_iv, $app_aes_key)
{
  // Set cipher method
  $cipherMethod = 'AES-256-CBC';

  $encrypted_data_iv = base64_decode($encrypted_data_base64_iv);

  $app_aes_iv = substr($encrypted_data_iv, 0, 16);
  $encrypted_data = substr($encrypted_data_iv, 16, (strlen($encrypted_data_iv) - 16));
  // Decryption
  $decrypted_data = openssl_decrypt($encrypted_data, $cipherMethod, $app_aes_key, OPENSSL_RAW_DATA, $app_aes_iv);

  $res = array(
    'app_aec_key' => $app_aes_key,
    'app_aec_iv' => $app_aes_iv,
    'encrypted_data' => $encrypted_data,
    'decrypted_data' => $decrypted_data,
    'encrypted_data_base64_iv' => $encrypted_data_base64_iv,
  );

  return $res;
}


//$mypay_service = '{"service_name":"api","cmd":"api\/quickpay"}';
$mypay_service = '{"service_name":"qrcode","cmd":"api\/quickpay"}';
$mypay_request_opts = array(
  'store_uid' => $mypay_store_uid,
  'name' => '測試刷卡',
  'cost' => '1000',
  'paymode' => '1,7,9',
  'buyer_info' => 'mail,builer_id.birthday,name_en'
);
$mypay_request_json = json_encode($mypay_request_opts);


$encrypt_service_all = mypay_encrypt($mypay_service, $app_aes_key);
$encrypt_service = $encrypt_service_all['encrypted_data_base64_iv'];

/*
 * test decrypted
$decrypt_service_all = mypay_decrypt($encrypt_service, $app_aes_key);

var_dump($encrypt_service_all);
var_dump($decrypt_service_all);
 */

$encrypt_payload_all = mypay_encrypt($mypay_request_json, $app_aes_key);
$encrypt_payload = $encrypt_payload_all['encrypted_data_base64_iv'];

/*
 * test decrypted
$decrypt_payload_all = mypay_decrypt($encrypt_payload, $app_aes_key);

var_dump($decrypt_payload_all);
var_dump($encrypt_payload_all);
 */

//
//
//
//
// test post API call
//
//


$post = array(
	"store_uid" => $mypay_store_uid,
    "service" => $encrypt_service,
    "encry_data" => $encrypt_payload
);
$ch = curl_init();
$options = array(
	CURLOPT_URL=>$mypay_api_url,
	CURLOPT_HEADER=>0,
	CURLOPT_VERBOSE=>0,
	CURLOPT_RETURNTRANSFER=>true,
	CURLOPT_USERAGENT=>"Mozilla/4.0 (compatible;)",
	CURLOPT_POST=>true,
	CURLOPT_POSTFIELDS=>http_build_query($post),
);
curl_setopt_array($ch, $options);

$result = curl_exec($ch);
curl_close($ch);

echo 'request URL = '. $mypay_api_url. PHP_EOL;
echo $result;
?>
